import re


class PunctuationAdapter:
    def __init__(self):
        self.__pattern = re.compile(r'''
                                     (?P<space_before>((\ +)(?P<bmark>[,.;:)\]])))
                                     | (?P<space_after>((?P<amark>[(\[])(\ +)))
                                     ''',
                                    re.VERBOSE)

    def replace(self, text):
        def _replace(matchobj):
            if matchobj.group('space_before'):
                return matchobj.group('bmark')
            elif matchobj.group('space_after'):
                return matchobj.group('amark')

        return self.__pattern.sub(_replace, text)
