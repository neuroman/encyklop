import re


class LatexAdapter:
    def __init__(self):
        self.__pattern = re.compile(r'''
                                     (?P<lower_quote>\u201e)  # „
                                     | (?P<upper_quote>\u201d)  # ”
                                     | (?P<dash>\u2013)  # –
                                     | (?P<ellipsis>\u2026) # …
                                     | (?P<apostrophe>')
                                     ''',
                                    re.VERBOSE,
                                    )

    def replace(self, text):
        def _replace(matchobj):
            if matchobj.group('lower_quote'):
                return '*'
            elif matchobj.group('upper_quote'):
                return "*"
            elif matchobj.group('dash'):
                return '--'
            elif matchobj.group('ellipsis'):
                return '\ldots\\'
            elif matchobj.group('apostrophe'):
                return '*'

        return self.__pattern.sub(_replace, text)
