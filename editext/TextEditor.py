from typing import Callable, List

from editext.TextFormatter import TextFormatter
from editext.adapters.Punctuation import PunctuationAdapter


class Paragraph:
    def __init__(self, text_lines, merge_lines):
        self.__text_lines = text_lines
        self.__merge_lines: Callable = merge_lines

    def modify(self, modifier):
        self.__text_lines = [modifier.apply(line) for line in self.__text_lines]

    def merge_into_text(self) -> str:
        return self.__merge_lines(self.__text_lines)


class TextEditor:
    def __init__(self, raw_text: List[str], text_formatter: TextFormatter, adapter=PunctuationAdapter()):
        self.__paragraphs = []
        if isinstance(raw_text, list):
            for raw_line in raw_text:
                raw_line = adapter.replace(raw_line)
                self.__paragraphs.append(
                    Paragraph(text_formatter.split_into_lines(raw_line), text_formatter.merge_into_text))
        else:
            raw_text = adapter.replace(raw_text)
            self.__paragraphs.append(
                Paragraph(text_formatter.split_into_lines(raw_text), text_formatter.merge_into_text))

        self.__text_formatter = text_formatter

    def modify(self, modifier):
        for paragraph in self.__paragraphs:
            paragraph.modify(modifier)

    @property
    def text(self):
        return self.__text_formatter.join(self.__paragraphs)
