from abc import ABC, abstractmethod
from typing import List, Any


class TextFormatter(ABC):
    @abstractmethod
    def split_into_lines(self, text: str):
        pass

    @abstractmethod
    def merge_into_text(self, lines: List[Any]) -> str:
        pass

    @abstractmethod
    def join(self, units: List[Any]) -> str:
        pass
