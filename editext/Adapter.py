class Adapter:
    def __init__(self, adapters):
        self.__adapters: list = adapters

    def replace(self, text):
        for adapter in self.__adapters:
            text = adapter.replace(text)
        return text
