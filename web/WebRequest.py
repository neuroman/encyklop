import sys
import urllib.request
from socket import timeout

from bs4 import BeautifulSoup


class HtmlItem:
    def __init__(self, item):
        self.__item = item

    def __getitem__(self, key):
        return self.__item[key]

    def to_string(self):
        return ' '.join(self.__item.stripped_strings)

    def find_first(self, name, attrs=None):
        return HtmlItem(self.__item.find(name, attrs))

    def find_all(self, name, attrs=None):
        return [HtmlItem(it) for it in self.__item.find_all(name, attrs)]


class HtmlEmptyItem:
    def __getitem__(self, key):
        return ''

    @staticmethod
    def to_string():
        return ''

    @staticmethod
    def find_first(name, attrs=None):
        return HtmlEmptyItem()

    @staticmethod
    def find_all(name, attrs=None):
        return []


class HtmlObj:
    def __init__(self, html_text):
        self.__bs_html_obj = BeautifulSoup(html_text, 'html.parser')

    def find_first(self, name, attrs=None, **nargv):
        result = self.__bs_html_obj.find(name, attrs, **nargv)
        return HtmlItem(result) if result else HtmlEmptyItem()

    def find_all(self, name, attrs=None, **nargv):
        return [HtmlItem(it) for it in self.__bs_html_obj.find_all(name, attrs, **nargv)]


class WebRequest:
    TIMEOUT = 3

    def __init__(self, url):
        try:
            req_obj = urllib.request.urlopen(url, timeout=self.TIMEOUT)
            self.__html_obj = HtmlObj(req_obj.read())
        except urllib.request.URLError as url_error:
            sys.stderr.write('Http request for: {}, failed (reason: {}).\n'.format(url, url_error.reason))
            sys.exit(1)
        except timeout:
            sys.stderr.write('Http request for: {}, failed (reason: timeout).\n'.format(url))
            sys.exit(1)

    def visit(self, scraper):
        scraper.scrape(self.__html_obj)


# TODO: singleton
class WebRequestFactory:
    def __init__(self):
        self.__http_requests = dict()

    def create(self, url):
        if url not in self.__http_requests:
            self.__http_requests[url] = WebRequest(url)
        return self.__http_requests[url]
