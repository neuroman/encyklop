from termline.Splitting import split_in_two_lines


def test_split_in_two_lines():
    def _split_in_two_lines(line):
        return split_in_two_lines(line, 10)

    assert ('-------', '-|--- ------') == _split_in_two_lines('------- -|--- ------')
    assert ('--- -----|', '-- ------') == _split_in_two_lines('--- -----| -- ------')
    assert ('------- -|--', '-------') == _split_in_two_lines('------- -|-- -------')
    assert ('------ --|--', '-------') == _split_in_two_lines('------ --|-- -------')
    assert ('------', '--|----------') == _split_in_two_lines('------ --|----------')
    assert ('---------|---', '------') == _split_in_two_lines('---------|--- ------')
    assert ('------- -', '- --------') == _split_in_two_lines('------- - - --------')
    assert ('---------|----------', '') == _split_in_two_lines('---------|----------')

    assert ('---------|', '') == _split_in_two_lines('---------|')
    assert ('----- ---|---', '') == _split_in_two_lines('----- ---|---')
    assert ('------', '--|---') == _split_in_two_lines('------ --|---')
    assert ('', '') == _split_in_two_lines('')
