import pytest
from mock import patch, Mock
from pwn.ActionFactory import PwnActionFactory


@pytest.fixture
def sjp_action(http_request):
    with patch('urllib.request.urlopen') as urlopen_mock:
        read_mock = Mock()
        read_mock.read.return_value = http_request
        urlopen_mock.return_value = read_mock

        factory = PwnActionFactory()
        return factory.create_sjp_action('', 120, True)


@pytest.mark.parametrize('html_file, expect_file', [
    ('tests/samples/ala.html', 'tests/samples/ala.txt'),
    ('tests/samples/kon.html', 'tests/samples/kon.txt'),
    ('tests/samples/jak.html', 'tests/samples/jak.txt'),
    ('tests/samples/mlotek.html', 'tests/samples/mlotek.txt')
], ids=['ala', 'kon', 'jak', 'mlotek'])
def test_sjp_action(sjp_action, expected, html_file, expect_file):
    assert expected == sjp_action.run()


@pytest.fixture
def sjp_synonym_action(http_request):
    with patch('urllib.request.urlopen') as urlopen_mock:
        read_mock = Mock()
        read_mock.read.return_value = http_request
        urlopen_mock.return_value = read_mock

        factory = PwnActionFactory()
        return factory.create_sjp_synonym_action('', 120)


@pytest.mark.parametrize('html_file, expect_file', [
    ('tests/samples/glupiec.html', 'tests/samples/synonimy_glupiec.txt'),
    ('tests/samples/jak.html', 'tests/samples/synonimy_jak.txt')
], ids=['glupiec', 'jak'])
def test_sjp_synonym_action(sjp_synonym_action, expected, html_file, expect_file):
    assert expected == sjp_synonym_action.run()


@pytest.fixture
def wso_action(http_request):
    with patch('urllib.request.urlopen') as urlopen_mock:
        read_mock = Mock()
        read_mock.read.return_value = http_request
        urlopen_mock.return_value = read_mock

        factory = PwnActionFactory()
        return factory.create_wso_action('', 120, True)


@pytest.mark.parametrize('html_file, expect_file', [
    ('tests/samples/zolty.html', 'tests/samples/zolty.txt')
], ids=['zloty'])
def test_wso_action(wso_action, expected, html_file, expect_file):
    assert expected == wso_action.run()
