from termline.TextModifiers import Justify


def test_justify_expand():
    text = ' Siała baba  mak, nie  wiedziała  jak.  '
    padding = 5
    base_line_length = len(text) - 1

    justify = Justify(base_line_length, padding)
    result = justify.apply(text)
    assert padding + base_line_length == len(result)

