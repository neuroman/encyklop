import pytest


@pytest.fixture
def http_request(html_file):
    with open(html_file, 'r') as smpl_dect:
        return smpl_dect.read()


@pytest.fixture
def expected(expect_file):
    with open(expect_file, 'r') as smpl_dect:
        return smpl_dect.read()
