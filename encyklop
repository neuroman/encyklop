#!/usr/bin/env python3

import sys
from argparse import ArgumentParser
from pwn.ActionFactory import PwnActionFactory
from termline.FormatConsts import LINE_WIDTH


def get_program_name_from(path):
    return path.split('/')[-1][0]


def build_commandline_parser(program_name):
    parser = ArgumentParser(prog=program_name)
    parser.add_argument('-o', '--wso', metavar='word', nargs='+')
    parser.add_argument('-s', '--sjp', metavar='word', nargs='+')
    parser.add_argument('-y', '--synonim', action='store_true')
    parser.add_argument('-r', '--random', action='store_true')
    parser.add_argument('--msr', action='store_true')
    parser.add_argument('-c', '--columns', metavar='number', dest='cols_num', nargs=1, type=int,
                        default=[LINE_WIDTH])
    parser.add_argument('-t', '--title', action='store_true', default=False)
    parser.add_argument('-e', '--epwn', metavar='word', nargs='+')
    parser.add_argument('-n', '--items', metavar='number', nargs=1, type=int, default=[3])
    return parser


def main():
    parser = build_commandline_parser(get_program_name_from(sys.argv[0]))
    option = parser.parse_args(sys.argv[1:])

    pwn_factory = PwnActionFactory()
    actions = list()
    if option.wso:
        actions.append(pwn_factory.create_wso_action(option.wso, option.cols_num[0], option.title))
    if option.sjp:
        actions.append(pwn_factory.create_sjp_action(option.sjp, option.cols_num[0], option.title))
        if option.synonim:
            actions.append(pwn_factory.create_sjp_synonym_action(option.sjp, option.cols_num[0]))
    if option.epwn:
        actions.append(pwn_factory.create_epwn_action(option.epwn, option.items[0]))
    if option.msr:
        actions.append(pwn_factory.create_random_action())

    for action in actions:
        print('{}'.format(action.run()), end='')


if __name__ == '__main__':
    main()
