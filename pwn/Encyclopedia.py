import sys

from editext.TextEditor import TextEditor
from pwn.Scraper import EPwnOutline, EPwnDefinition
from termline.TextModifiers import Justify


class OutlineIndex:
    def __init__(self, outline):
        self.__outline = outline
        self.__index = None
        self.__iterator = None

    def __iter__(self):
        self.__index = 0
        self.__iterator = iter(self.__outline)
        return self

    def __next__(self):
        self.__index += 1
        return self.__index, next(self.__iterator)

    def __getitem__(self, index):
        return self.__outline[index - 1]

    def empty(self):
        return len(self.__outline) == 0


class Outline:
    def __init__(self, web_req_factory, url, items_limit, action_result):
        self.__web_req_factory = web_req_factory
        self.__url = url
        self.__items_limit = items_limit
        self.__action_result: dict = action_result

    def fetch(self):
        outline = EPwnOutline(self.__items_limit)
        http_request = self.__web_req_factory.create(self.__url)
        http_request.visit(outline)
        self.__action_result['outline_index'] = OutlineIndex(outline.get_items())


class UserChoice:
    def __init__(self, terminal_text, action_result):
        self.__terminal_text = terminal_text
        self.__action_result: dict = action_result

    def fetch(self):
        if 'outline_index' in self.__action_result:
            outline_index = self.__action_result['outline_index']
            if not outline_index.empty():
                text_editor = TextEditor(
                    ['({}) {}'.format(pos, outline.text) for pos, outline in outline_index], self.__terminal_text)

                try:
                    print(text_editor.text, end='')
                    answer = input('\n\n\tCo Cię interesuje (1, 2, etc., z[akończ])? ')
                    if answer == 'z':
                        raise KeyboardInterrupt()

                    self.__action_result['synopsis'] = outline_index[int(answer)].text
                    self.__action_result['definition_url'] = outline_index[int(answer)].url
                except (ValueError, KeyboardInterrupt, IndexError):
                    sys.exit(0)


class Definition:
    def __init__(self, web_req_factory, action_result):
        self.__web_req_factory = web_req_factory
        self.__action_result: dict = action_result

    def fetch(self):
        if 'definition_url' in self.__action_result:
            definition = EPwnDefinition()
            http_request = self.__web_req_factory.create(self.__action_result['definition_url'])
            http_request.visit(definition)
            self.__action_result['definition'] = definition.get_raw_web_text()


class EncyclopediaAction:
    def __init__(self, web_request_factory, url, terminal_text, terminal_text_user_choice, items_limit):
        self.__action_result = dict()

        self.__actions = [Outline(web_request_factory, url, items_limit, self.__action_result),
                          UserChoice(terminal_text_user_choice, self.__action_result),
                          Definition(web_request_factory, self.__action_result)]

        self.__terminal_text = terminal_text

    def run(self):
        for action in self.__actions:
            action.fetch()

        if 'definition' in self.__action_result:
            text_editor = TextEditor([self.__action_result['synopsis']] + self.__action_result['definition'],
                                     self.__terminal_text)
            text_editor.modify(Justify(base_line_length=110, padding=17))
            return text_editor.text

        return ''
