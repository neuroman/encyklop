import re


class DefinitionAdapter:
    def __init__(self):
        self.__pattern1 = re.compile(r'''
                                     (?P<def>(?P<beg_def>Definicja\ *[0-9]*:\ )(?P<body_def>.*))
                                     | (?P<remark>(?P<head>Uwagi\ językoznawcy)(?P<initials>\ *\[.*\]))
                                     ''',
                                     re.VERBOSE,
                                     )

        self.__pattern2 = re.compile(r'''
                                     (?P<end_def>,\u00bb)
                                     ''',
                                     re.VERBOSE,
                                     )

    def replace(self, text):
        def _replace1(matchobj):
            if matchobj.group('def'):
                return "\u00ab{}\u00bb".format(matchobj.group('body_def'))
            elif matchobj.group('remark'):
                return matchobj.group('head')

        def _replace2(matchobj):
            if matchobj.group('end_def'):
                return '\u00bb'

        text = self.__pattern1.sub(_replace1, text)
        return self.__pattern2.sub(_replace2, text)
