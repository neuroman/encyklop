from editext.TextEditor import TextEditor
from editext.adapters.Latex import LatexAdapter
from editext.adapters.Punctuation import PunctuationAdapter
from editext.Adapter import Adapter
from pwn.msr.DefinitionAdapter import DefinitionAdapter
from pwn.msr.Scraper import PaginationScraper, OnPageWordsLinksScraper, MSRWordScraper
from termline.TerminalText import TerminalText


def decorate_word(msr):
    print(msr.word)
    return ['* **{}**'.format(msr.word)] + msr.definition


class _Action:
    def __init__(self, web_req_factory, scraper):
        self.__web_req_factory = web_req_factory
        self.__scraper = scraper

    def fetch(self, url):
        web_req = self.__web_req_factory.create(url)
        web_req.visit(self.__scraper)

    @property
    def scraper(self):
        return self.__scraper


class MSRAction:
    URL = 'https://sjp.pwn.pl/ciekawostki/Galeria-Mlodziezowe-slowo-roku;200004.html'

    def __init__(self, web_req_factory):
        self.__pagination = _Action(web_req_factory, PaginationScraper())
        self.__on_page_words_links = _Action(web_req_factory, OnPageWordsLinksScraper())
        self.__msr_word = _Action(web_req_factory, MSRWordScraper())
        self.__adapter = Adapter([DefinitionAdapter(), PunctuationAdapter(), LatexAdapter()])

    def run(self):
        msr = []
        self.__pagination.fetch(self.URL)
        for on_page_link in self.__pagination.scraper.get_links():
            self.__on_page_words_links.fetch(on_page_link)
            for word_link in self.__on_page_words_links.scraper.get_links():
                self.__msr_word.fetch(word_link)
                msr.extend(decorate_word(self.__msr_word.scraper.get_word()))
        text_editor = TextEditor(msr, TerminalText(indent_first=2, left_margin=2), self.__adapter)

        with open('mlodziezowe_slowo_roku.md', 'w') as desc:
            desc.write(text_editor.text)
        return ''
