import re
from dataclasses import dataclass
from typing import List


class PaginationScraper:
    def __init__(self):
        self.__links = []

    def scrape(self, html_obj):
        parent = html_obj.find_first('div', {'class': 'col-sm-12 col-md-5 col-lg-6 search-content'})
        pagination = parent.find_first('ul', {'class': 'pagination'})
        self.__links = [a['href'] for a in filter(lambda a: a.to_string().isdigit(), pagination.find_all('a'))]

    def get_links(self):
        return self.__links


class OnPageWordsLinksScraper:
    def __init__(self):
        self.__links = []

    def scrape(self, html_obj):
        parent = html_obj.find_first('div', {'class': 'col-sm-12 col-md-5 col-lg-6 search-content'})
        units = parent.find_all('li', {'class': 'type-187151'})
        anchors = [unit.find_first('a', {'class': 'anchor-title'}) for unit in units]
        self.__links = [a['href'] for a in anchors]

    def get_links(self):
        return self.__links


@dataclass(eq=False, repr=False)
class MSR:
    word: str
    definition: List[str]

    def apply(self, modify: callable):
        self.definition = list(map(lambda value: modify(value), self.definition))


class MSRWordScraper:
    PATTERN = re.compile(r'''
                          ^Słowo\ zgłoszone\ w\ plebiscycie\ na\ Młodzieżowe\ słowo\ roku\ [0-9]* |
                          ^Dodano:\ [0-9]* |
                          ^Powrót\ do\ galerii
                          ''',
                         re.VERBOSE)

    def __init__(self):
        self.__msr = None

    def scrape(self, html_obj):
        def _cleaner(unit):
            text = unit.to_string()
            if text and not self.PATTERN.search(text):
                return True
            return False

        parent = html_obj.find_first('div', {'class': 'col-sm-12 col-md-5 col-lg-6 search-content'})
        word = parent.find_first('h1', {'id': re.compile(r'haslo-[0-9]*')})
        units = parent.find_all('div', {'class': 'tekst-para'})
        self.__msr = MSR(word.to_string(), [text.to_string() for text in filter(_cleaner, units)])

    def get_word(self):
        return self.__msr
