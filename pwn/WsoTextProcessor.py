import re
from typing import Union, List


def clean_raw_web_text(line):
    def replace(matchobj):
        if matchobj.group('space_semicolon'):
            return ';'
        elif matchobj.group('space_colon'):
            return ''

    pattern = re.compile(r'''
                           (?P<space_semicolon>\ +;)
                         | (?P<space_colon>\ +,)
                         ''',
                         re.VERBOSE)

    return pattern.sub(replace, line)


def process_wso_text(raw_text: Union[List[str], str],  title: bool = False):
    raw_text = raw_text if isinstance(raw_text, list) else [raw_text]

    text_lines = []
    if raw_text and title:
        text_lines.append('WIELKI SŁOWNIK ORTOGRAFICZNY')
    for line in raw_text:
        text_lines.append(clean_raw_web_text(line))

    return text_lines
