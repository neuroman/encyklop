import re
from typing import Union, List, Any


BREAK_MARK = 'C0DED00D'


def separate_enumeration_items(line):
    pattern = re.compile(r'(?P<new_point> *\d+\. +\D *\S+)')
    return pattern.sub(BREAK_MARK + r'\g<new_point>', line)


def separate_derivatives(line):
    return re.sub(r' \u2022', BREAK_MARK + 'DERYWATY:', line, count=1)


def clean_raw_web_text(line):
    def replace(matchobj):
        if matchobj.group('space') and matchobj.group('sep'):
            return matchobj.group('sep')
        elif matchobj.group('bold_dot_after_def'):
            return '\u00bb'
        elif matchobj.group('bold_dot'):
            return ','

    pattern = re.compile(r'''
                          (?P<space>\ +)(?P<sep>[.,\u00bb])
                         | (?P<bold_dot_after_def>\u00bb\ +\u2022)
                         | (?P<bold_dot>\ +\u2022)
                         ''',
                         re.VERBOSE)

    return pattern.sub(replace, line)


class SjpAdapter:
    def __init__(self):
        self.__pattern1 = re.compile(r'''
                                     (?P<space>\ +)(?P<sep>[.,\u00bb])
                                     | (?P<bold_dot_after_def>\u00bb\ +\u2022)
                                     | (?P<bold_dot>\ +\u2022)
                                     ''',
                                     re.VERBOSE)

        self.__split_enumerations = re.compile(r'(?P<new_point> *\d+\. +\D *\S+)')

    def replace(self, text):
        def _replace(matchobj):
            if matchobj.group('space') and matchobj.group('sep'):
                return matchobj.group('sep')
            elif matchobj.group('bold_dot_after_def'):
                return '\u00bb'
            elif matchobj.group('bold_dot'):
                return ','

        text = re.sub(r' \u2022', BREAK_MARK + 'DERYWATY:', text, count=1)
        text = self.__pattern1.sub(_replace, text)
        return self.__split_enumerations.sub(BREAK_MARK + r'\g<new_point>', text)


def process_sjp_text(raw_text:  Union[List[str], str], title: bool = False):
    raw_text = raw_text if isinstance(raw_text, list) else [raw_text]

    text_lines = []
    adapter = SjpAdapter()
    if raw_text and title:
        text_lines.append('SŁOWNIK JĘZYKA POLSKIEGO')
    for line in raw_text:
        line = adapter.replace(line)
        text_lines.extend(line.split(BREAK_MARK))

    return text_lines


def process_synonyms_text(synonyms: List[Any], title: bool = True):

    text_lines = []
    if synonyms and title:
        text_lines.append('SYNONIMY')
    for synonym in synonyms:
        text_lines.append('{}: {}'.format(' '.join(synonym.title), ', '.join(synonym.variants)))

    return text_lines
