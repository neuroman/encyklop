import re
from collections import namedtuple


def convert_html_to_text(get_raw_web_text):
    def decorate(self):
        text = list()
        for item in get_raw_web_text(self):
            xitem = item.to_string()
            if xitem:
                text.append(xitem)
        return text
    return decorate


class SjpScraper:
    def __init__(self):
        self.__html_items = list()

    def scrape(self, html_obj):
        self.__html_items = html_obj.find_all('div', {'class': 'ribbon-element type-187126'})

    @convert_html_to_text
    def get_raw_web_text(self):
        return self.__html_items


Synonym = namedtuple('Synonym', ['title', 'variants'])


class SjpSynonymScraper:
    def __init__(self):
        self.__html_items = list()

    def scrape(self, html_obj):
        synonym_section = html_obj.find_first('div', {'class': 'row col-wrapper visible-xs visible-sm'})
        synonyms = synonym_section.find_all('div', {'class': 'ribbon-element type-187102'})
        for iter in synonyms:
            self.__html_items.append({'title': iter.find_all('span'),
                                      'variants': iter.find_all('li')})

    def get_items(self):
        def extract_raw_web_text(tags):
            values = []
            for iter in tags:
                value = iter.to_string()
                if value:
                    values.append(value)
            return values

        synonyms = []
        for iter in self.__html_items:
            title_and_gloss = extract_raw_web_text(iter['title'])
            if title_and_gloss:
                variants = extract_raw_web_text(iter['variants'])
                synonyms.append(Synonym(title_and_gloss, variants))
        return synonyms


class WsoScraper:
    def __init__(self):
        self.__html_items = list()

    def scrape(self, html_obj):
        self.__html_items = html_obj.find_all('div', {'class': 'ribbon-element type-187125'})

    @convert_html_to_text
    def get_raw_web_text(self):
        return self.__html_items


Outline = namedtuple('Outline', ['text', 'url'])


class EPwnOutline:
    def __init__(self, items_limit):
        self.__items_limit = items_limit
        self.__items = list()

    def scrape(self, html_obj):
        html_items = html_obj.find_all('div', {'class': 'ribbon-element type-187142'}, limit=self.__items_limit)

        for item in html_items:
            anchor = item.find_first('a', {'class': 'anchor-title'})
            self.__items.append(Outline(item.to_string(), anchor['href']))

    def get_items(self):
        return self.__items


class EPwnDefinition:
    def __init__(self):
        self.__html_items = []

    def scrape(self, html_obj):
        html_item = html_obj.find_first('div', {'class': 'enc-haslo enc-haslo-tresc enc-anchor'})
        self.__html_items.extend(
            html_item.find_all(re.compile(r'(div)|(span)'), {'class': re.compile(r'(tekst-*)|(tytul-tem)')}))

    @convert_html_to_text
    def get_raw_web_text(self):
        return self.__html_items

