from urllib.parse import quote

from pwn.Dictionary import SjpAction, SjpSynonymAction
from pwn.Encyclopedia import EncyclopediaAction
from pwn.Scraper import WsoScraper, SjpScraper
from pwn.SjpTextProcessor import process_sjp_text
from pwn.WsoTextProcessor import process_wso_text
from pwn.msr.MSRAction import MSRAction
from termline.TerminalText import TerminalText
from editext.TextEditor import TextEditor
from web.WebRequest import WebRequestFactory


def compile_url(base_url, query_phrase):
    return base_url + quote(' '.join(query_phrase)) + '.html'


class PwnActionFactory:
    def __init__(self):
        self.__web_req_factory = WebRequestFactory()

    def create_wso_action(self, query_phrase, line_width, title=False):
        url = compile_url('https://sjp.pwn.pl/szukaj/', query_phrase)
        text_formatter = TerminalText(line_width=line_width)
        return SjpAction(self.__web_req_factory.create(url), WsoScraper(),
                         lambda text: TextEditor(process_wso_text(text, title), text_formatter))

    def create_sjp_action(self, query_phrase, line_width, title=False):
        url = compile_url('https://sjp.pwn.pl/szukaj/', query_phrase)
        text_formatter = TerminalText(line_width=line_width)
        return SjpAction(self.__web_req_factory.create(url), SjpScraper(),
                         lambda text: TextEditor(process_sjp_text(text, title), text_formatter))

    def create_sjp_synonym_action(self, query_phrase, line_width):
        url = compile_url('https://sjp.pwn.pl/szukaj/', query_phrase)
        terminal_text = TerminalText(line_width=line_width)
        return SjpSynonymAction(self.__web_req_factory.create(url), terminal_text)

    def create_epwn_action(self, query_phrase, item_limits):
        url = compile_url('https://encyklopedia.pwn.pl/szukaj/', query_phrase)
        terminal_text = TerminalText(line_width=118, indent_first=5, left_margin=5)
        terminal_text_user_choice = TerminalText(line_width=110, indent_first=4, left_margin=8, bottom_margin=0)
        return EncyclopediaAction(self.__web_req_factory, url, terminal_text, terminal_text_user_choice, item_limits)

    def create_random_action(self):
        return MSRAction(self.__web_req_factory)
