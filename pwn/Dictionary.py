from pwn.Scraper import SjpSynonymScraper
from pwn.SjpTextProcessor import process_synonyms_text
from editext.TextEditor import TextEditor


class SjpAction:
    def __init__(self, web_request, scraper, create_text_editor):
        self.__web_request = web_request
        self.__scraper = scraper
        self.__create_text_editor = create_text_editor

    def run(self):
        self.__web_request.visit(self.__scraper)
        text_editor = self.__create_text_editor(self.__scraper.get_raw_web_text())
        return text_editor.text


class SjpSynonymAction:
    def __init__(self, web_request, terminal_text):
        self.__web_request = web_request
        self.__terminal_text = terminal_text

    def run(self):
        scraper = SjpSynonymScraper()
        self.__web_request.visit(scraper)
        text_editor = TextEditor(process_synonyms_text(scraper.get_items()), self.__terminal_text)
        return text_editor.text
