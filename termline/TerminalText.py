from typing import List, Any

from editext.TextFormatter import TextFormatter
from termline.FormatConsts import *
from termline.Splitting import split_into_lines
from termline.TerminalLine import TerminalLine


class TerminalText(TextFormatter):
    def __init__(self, line_width=LINE_WIDTH, indent_first=INDENT_FIRST, left_margin=LEFT_MARGIN, top_margin=VMARGIN,
                 bottom_margin=VMARGIN, interunit=VMARGIN):
        self.__line_width = line_width
        self.__indent_first = ' ' * indent_first
        self.__left_margin = ' ' * left_margin
        self.__text_template = '{}{}{}'.format('\n' * top_margin, '{}', '\n' * bottom_margin)
        self.__interunit = interunit

    def split_into_lines(self, text: str):
        def create_term_line(line):
            indent = (len(line) - len(line.lstrip())) * ' '
            return TerminalLine(indent, line.lstrip())

        if len(self.__indent_first) + len(text) <= self.__line_width:
            return [TerminalLine(self.__indent_first, text)]
        else:
            return list(map(create_term_line,
                            split_into_lines(text, self.__line_width, self.__indent_first, self.__left_margin)))

    def merge_into_text(self, lines: List[Any]) -> str:
        return '{}\n'.format('\n'.join(map(str, lines)))

    def join(self, units: List[Any]) -> str:
        text = ('\n' * self.__interunit).join([unit.merge_into_text() for unit in units])

        if text:
            return self.__text_template.format(text)
        return ''
