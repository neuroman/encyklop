import random


class Justify:
    def __init__(self, base_line_length=120, padding=10):
        self.__base_line_len = base_line_length
        self.__padding = padding

    def apply(self, text):
        def find_all_spaces(line, start=0):
            index = line[start:].find(' ')
            if index >= 0:
                index += start
                return [index] + find_all_spaces(line, index + 1)
            return []

        def justify(line):
            spaces = find_all_spaces(line)
            while len(line) < self.__base_line_len + self.__padding:
                idx_of_sp = random.randint(0, len(spaces) - 1)
                idx_of_sp_in_str = spaces[idx_of_sp]
                line = line[:idx_of_sp_in_str + 1] + ' ' + line[idx_of_sp_in_str + 1:]
                spaces = spaces[:idx_of_sp + 1] + [idx + 1 for idx in spaces[idx_of_sp + 1:]]
            return line

        if len(text) >= self.__base_line_len:
            return justify(text.strip())

        return text
