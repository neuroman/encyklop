from typing import List


def split_in_two_lines(line, line_base_len):
    if len(line) > line_base_len:
        lline_break_idx = line[:line_base_len].rfind(' ')
        rline_break_idx = line[line_base_len:].find(' ')

        if lline_break_idx < 0 and rline_break_idx < 0:
            return line, ''

        if lline_break_idx >= 0 and rline_break_idx >= 0:
            if (line_base_len - lline_break_idx - 1) < rline_break_idx:
                line_base_len = lline_break_idx
            else:
                line_base_len += rline_break_idx
        elif lline_break_idx >= 0:
            if (line_base_len - lline_break_idx - 1) > (len(line) - line_base_len):
                line_base_len = len(line)
            else:
                line_base_len = lline_break_idx
        else:
            line_base_len += rline_break_idx

        return line[:line_base_len], line[line_base_len + 1:]

    return line, ''


def split_into_lines(line, line_base_len, first_indent, indent) -> List[str]:
    def _split_in_two_lines(l):
        return split_in_two_lines(l, line_base_len)

    def _impl(l):
        if not l:
            return []
        lline, rline = _split_in_two_lines(l)
        if indent and rline:
            rline = indent + rline
        if len(rline) > line_base_len:
            return [lline] + _impl(rline)
        if not rline:
            return [lline]
        return [lline, rline]

    return _impl(first_indent + line)
