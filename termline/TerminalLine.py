class TerminalLine:
    def __init__(self, indent, line):
        self.__indent: str = indent
        self.__line: str = line

    def __getitem__(self, idx):
        if isinstance(idx, slice):
            return TerminalLine(self.__indent, self.__line[idx])
        return self.__line[idx]

    def __setitem__(self, idx, value):
        self.__line[idx] = value

    def __add__(self, other):
        if isinstance(other, TerminalLine):
            self.__line += other.__line
        else:
            self.__line += other
        return self

    def __len__(self):
        return len(self.__indent) + len(self.__line)

    def __str__(self):
        return self.__indent + self.__line

    def strip(self):
        return TerminalLine(self.__indent, self.__line.rstrip())

    def find(self, what):
        return self.__line.find(what)